package com.example.am.mykamus;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.am.mykamus.Adapter.KamusIEAdapter;
import com.example.am.mykamus.Database.KamusHelper;
import com.example.am.mykamus.Model.KamusModelIE;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IndonesiaEnglishFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IndonesiaEnglishFragment extends Fragment {

    public static final String TAG = "dbquery";

    RecyclerView recyclerView;
    SearchView searchView;
    KamusIEAdapter kamusAdapter;
    KamusHelper kamusHelper;

    List<KamusModelIE> kamusModelIES = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public IndonesiaEnglishFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IndonesiaEnglishFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IndonesiaEnglishFragment newInstance() {
        IndonesiaEnglishFragment fragment = new IndonesiaEnglishFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_indonesia_english, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        searchView = (SearchView) view.findViewById(R.id.search_view);

        searchView.setVisibility(View.VISIBLE);
        searchView.setIconified(false);
//        searchView.setFocusable(true);
        searchView.clearFocus();
//        searchView.requestFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!s.equals("")) {
                    kamusHelper.open();
//                    ArrayList<KamusModelIE> kamusModelIES = kamusHelper.getDataByNameIE(s);
//                    kamusModelIES.addAll(kamusHelper.getDataByNameIE(s));
                    kamusModelIES = kamusHelper.getDataByNameIE(s);
                    kamusHelper.close();
                    kamusAdapter.addItem(kamusModelIES);
                    kamusAdapter.notifyDataSetChanged();
                }else {
                    GetAllData();
                }
                return true;
            }
        });

        kamusAdapter = new KamusIEAdapter(getActivity());
        kamusHelper = new KamusHelper(getActivity());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(kamusAdapter);
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent = new Intent(getActivity(), DetailKamusActivity.class);
                String kata = kamusModelIES.get(position).getKata();
                String makna = kamusModelIES.get(position).getMakna();
                intent.putExtra("kata", kata);
                intent.putExtra("makna", makna);
                startActivity(intent);
            }
        });

        GetAllData();
        return view;
    }

    private void GetAllData() {
        kamusHelper.open();
        kamusModelIES = kamusHelper.getAllDataIE();
        kamusHelper.close();
        kamusAdapter.addItem(kamusModelIES);
    }

}
