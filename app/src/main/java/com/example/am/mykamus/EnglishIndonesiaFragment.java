package com.example.am.mykamus;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.am.mykamus.Adapter.KamusEIAdapter;
import com.example.am.mykamus.Database.KamusHelper;
import com.example.am.mykamus.Model.KamusModelEI;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EnglishIndonesiaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EnglishIndonesiaFragment extends Fragment {

    RecyclerView recyclerView;
    SearchView searchView;
    KamusEIAdapter adapter;
    KamusHelper kamusHelper;
    ArrayList<KamusModelEI> kamusModelEIS;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public EnglishIndonesiaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EnglishIndonesiaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EnglishIndonesiaFragment newInstance(String param1, String param2) {
        EnglishIndonesiaFragment fragment = new EnglishIndonesiaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_english_indonesia, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        searchView = (SearchView) view.findViewById(R.id.search_view);

        searchView.setVisibility(View.VISIBLE);
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!s.equals("")) {
                    kamusHelper.open();
                    kamusModelEIS = kamusHelper.getDataByNameEI(s);
                    kamusHelper.close();
                    adapter.addItemEI(kamusModelEIS);
                    adapter.notifyDataSetChanged();
                }else {
                    GetAllDataEI();
                }
                return true;            }
        });

        kamusHelper = new KamusHelper(getActivity());
        adapter = new KamusEIAdapter(getActivity());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent = new Intent(getActivity(), DetailKamusActivity.class);
                String kata = kamusModelEIS.get(position).getKata();
                String makna = kamusModelEIS.get(position).getMakna();
                intent.putExtra("kata", kata);
                intent.putExtra("makna", makna);
                startActivity(intent);
            }
        });

        GetAllDataEI();
        return view;
    }

    public void GetAllDataEI() {
        kamusHelper.open();
        kamusModelEIS = kamusHelper.getAllDataEI();
        kamusHelper.close();
        adapter.addItemEI(kamusModelEIS);
    }
}
