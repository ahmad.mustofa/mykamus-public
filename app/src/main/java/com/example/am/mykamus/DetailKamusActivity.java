package com.example.am.mykamus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class DetailKamusActivity extends AppCompatActivity {

    TextView tvKata, tvMakna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kamus);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Kamus");

        Bundle bundle = getIntent().getExtras();
        String kata = bundle.getString("kata");
        String makna = bundle.getString("makna");

        tvKata = (TextView) findViewById(R.id.tv_kata);
        tvMakna = (TextView) findViewById(R.id.tv_makna);

        tvKata.setText(kata);
        tvMakna.setText(makna);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
