package com.example.am.mykamus.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class KamusModelIE implements Parcelable {

    private int id;
    private String kata;
    private String makna;

    public KamusModelIE() {
    }

    public KamusModelIE(String kata, String makna) {
        this.kata = kata;
        this.makna = makna;
    }

    public KamusModelIE(int id, String kata, String makna) {
        this.id = id;
        this.kata = kata;
        this.makna = makna;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKata() {
        return kata;
    }

    public void setKata(String kata) {
        this.kata = kata;
    }

    public String getMakna() {
        return makna;
    }

    public void setMakna(String makna) {
        this.makna = makna;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.kata);
        dest.writeString(this.makna);
    }

    protected KamusModelIE(Parcel in) {
        this.id = in.readInt();
        this.kata = in.readString();
        this.makna = in.readString();
    }

    public static final Parcelable.Creator<KamusModelIE> CREATOR = new Parcelable.Creator<KamusModelIE>() {
        @Override
        public KamusModelIE createFromParcel(Parcel source) {
            return new KamusModelIE(source);
        }

        @Override
        public KamusModelIE[] newArray(int size) {
            return new KamusModelIE[size];
        }
    };
}
