package com.example.am.mykamus.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.example.am.mykamus.Model.KamusModelEI;
import com.example.am.mykamus.Model.KamusModelIE;

import java.util.ArrayList;
import java.util.List;

import static android.provider.BaseColumns._ID;
import static com.example.am.mykamus.Database.DatabaseContract.KamusColumn.KATA;
import static com.example.am.mykamus.Database.DatabaseContract.KamusColumn.MAKNA;
import static com.example.am.mykamus.Database.DatabaseContract.TABLE_NAME_EI;
import static com.example.am.mykamus.Database.DatabaseContract.TABLE_NAME_IE;
import static com.example.am.mykamus.IndonesiaEnglishFragment.TAG;

public class KamusHelper {
    private Context context;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    public KamusHelper(Context context) {
        this.context = context;
    }

    public KamusHelper open() throws SQLException {
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        databaseHelper.close();
    }

    public void beginTransaction() {
        database.beginTransaction();
    }

    public void setTransactionSuccess() {
        database.setTransactionSuccessful();
    }

    public void endTransaction() {
        database.endTransaction();
    }

    public void insertTransactionIE(KamusModelIE kamusModelIE) {
        String sql_ie = "insert into " + TABLE_NAME_IE + " (" + KATA + ", " + MAKNA
                + ") VALUES (?, ?)";
//        String sql_ei = "insert into " + TABLE_NAME_EI + " (" + KATA + ", " + MAKNA
//                + ") VALUES (?, ?)";

        SQLiteStatement stmt1 = database.compileStatement(sql_ie);
//        SQLiteStatement stmt2 = database.compileStatement(sql_ei);

        stmt1.bindString(1, kamusModelIE.getKata());
        stmt1.bindString(2, kamusModelIE.getMakna());
        stmt1.execute();
        stmt1.clearBindings();

//        stmt2.bindString(1, kamusModelIE.getKata());
//        stmt2.bindString(2, kamusModelIE.getMakna());
//        stmt2.execute();
//        stmt2.clearBindings();
    }

    public void insertTransactionEI(KamusModelEI kamusModelEI) {

        String sql_ei = "insert into " + TABLE_NAME_EI + " (" + KATA + ", " + MAKNA
                + ") VALUES (?, ?)";

        SQLiteStatement stmt2 = database.compileStatement(sql_ei);

        stmt2.bindString(1, kamusModelEI.getKata());
        stmt2.bindString(2, kamusModelEI.getMakna());
        stmt2.execute();
        stmt2.clearBindings();
    }

    public ArrayList<KamusModelIE> getDataByName(String nama, String type) {
        String result = "";
        Cursor cursor = database.query(type, null, KATA + " LIKE ?", new String[]{nama}, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<KamusModelIE> arrayList = new ArrayList<>();
        KamusModelIE kamusModelIE;
        if (cursor.getCount() > 0) {
            do {
                kamusModelIE = new KamusModelIE();
                kamusModelIE.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                kamusModelIE.setMakna(cursor.getString(cursor.getColumnIndexOrThrow(MAKNA)));
                kamusModelIE.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));

                arrayList.add(kamusModelIE);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public List<KamusModelIE> getDataByNameIE(String nama) {
        String result = "";
        Cursor cursor = database.query(TABLE_NAME_IE, null, KATA + " LIKE ?",new String[]{nama+"%"}, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        List<KamusModelIE> arrayList = new ArrayList<>();
        KamusModelIE kamusModelIE;
        if (cursor.getCount() > 0) {
            do {
                kamusModelIE = new KamusModelIE();
                kamusModelIE.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                kamusModelIE.setMakna(cursor.getString(cursor.getColumnIndexOrThrow(MAKNA)));
                kamusModelIE.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));

                arrayList.add(kamusModelIE);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }

        Log.d(TAG, DatabaseUtils.dumpCursorToString(cursor));

        cursor.close();
        return arrayList;
    }

    public ArrayList<KamusModelEI> getDataByNameEI(String nama) {
        String result = "";
        Cursor cursor = database.query(TABLE_NAME_EI, null, KATA + " LIKE ?", new String[]{nama+"%"}, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<KamusModelEI> arrayList = new ArrayList<>();
        KamusModelEI kamusModelEI;
        if (cursor.getCount() > 0) {
            do {
                kamusModelEI = new KamusModelEI();
                kamusModelEI.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                kamusModelEI.setMakna(cursor.getString(cursor.getColumnIndexOrThrow(MAKNA)));
                kamusModelEI.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));

                arrayList.add(kamusModelEI);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<KamusModelIE> getAllDataIE() {
        Cursor cursor = database.query(TABLE_NAME_IE, null, null, null, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<KamusModelIE> arrayList = new ArrayList<>();
        KamusModelIE kamusModelIE;
        if (cursor.getCount() > 0) {
            do {
                kamusModelIE = new KamusModelIE();
                kamusModelIE.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                kamusModelIE.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
                kamusModelIE.setMakna(cursor.getString(cursor.getColumnIndexOrThrow(MAKNA)));

                arrayList.add(kamusModelIE);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<KamusModelEI> getAllDataEI() {
        Cursor cursor = database.query(TABLE_NAME_EI, null, null, null, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<KamusModelEI> arrayList = new ArrayList<>();
        KamusModelEI kamusModelEI;
        if (cursor.getCount() > 0) {
            do {
                kamusModelEI = new KamusModelEI();
                kamusModelEI.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                kamusModelEI.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
                kamusModelEI.setMakna(cursor.getString(cursor.getColumnIndexOrThrow(MAKNA)));

                arrayList.add(kamusModelEI);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

}
