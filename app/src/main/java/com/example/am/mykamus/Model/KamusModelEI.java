package com.example.am.mykamus.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class KamusModelEI implements Parcelable {

    private int id;
    private String kata;
    private String makna;

    public KamusModelEI() {
    }

    public KamusModelEI(String kata, String makna) {
        this.kata = kata;
        this.makna = makna;
    }

    public KamusModelEI(int id, String kata, String makna) {
        this.id = id;
        this.kata = kata;
        this.makna = makna;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKata() {
        return kata;
    }

    public void setKata(String kata) {
        this.kata = kata;
    }

    public String getMakna() {
        return makna;
    }

    public void setMakna(String makna) {
        this.makna = makna;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.kata);
        dest.writeString(this.makna);
    }

    protected KamusModelEI(Parcel in) {
        this.id = in.readInt();
        this.kata = in.readString();
        this.makna = in.readString();
    }

    public static final Parcelable.Creator<KamusModelEI> CREATOR = new Parcelable.Creator<KamusModelEI>() {
        @Override
        public KamusModelEI createFromParcel(Parcel source) {
            return new KamusModelEI(source);
        }

        @Override
        public KamusModelEI[] newArray(int size) {
            return new KamusModelEI[size];
        }
    };
}
