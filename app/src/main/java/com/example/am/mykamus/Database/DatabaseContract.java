package com.example.am.mykamus.Database;

import android.provider.BaseColumns;

public class DatabaseContract {
    static String TABLE_NAME_IE = "table_kamus_ie";
    static String TABLE_NAME_EI = "table_kamus_ei";

    static final class KamusColumn implements BaseColumns {
        static String KATA = "KATA";
        static String MAKNA = "MAKNA";
    }
}
