package com.example.am.mykamus.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static com.example.am.mykamus.Database.DatabaseContract.KamusColumn.KATA;
import static com.example.am.mykamus.Database.DatabaseContract.KamusColumn.MAKNA;
import static com.example.am.mykamus.Database.DatabaseContract.TABLE_NAME_EI;
import static com.example.am.mykamus.Database.DatabaseContract.TABLE_NAME_IE;

public class DatabaseHelper extends SQLiteOpenHelper{

    private static String DATABASE_NAME = "dbkamus";
    private static int DATABASE_VERSION = 1;

    public static String CREATE_TABLE_KAMUS_IE = "create table " + TABLE_NAME_IE +
            " (" + _ID + " integer primary key autoincrement, " +
            KATA + " text not null, " +
            MAKNA + " text not null);";

    public static String CREATE_TABLE_KAMUS_EI = "create table " + TABLE_NAME_EI +
            " (" + _ID + " integer primary key autoincrement, " +
            KATA + " text not null, " +
            MAKNA + " text not null);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_KAMUS_IE);
        db.execSQL(CREATE_TABLE_KAMUS_EI);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME_IE);
        db.execSQL("drop table if exists " + TABLE_NAME_EI);
        onCreate(db);
    }
}
