package com.example.am.mykamus.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.am.mykamus.Model.KamusModelIE;
import com.example.am.mykamus.R;

import java.util.ArrayList;
import java.util.List;

public class KamusIEAdapter extends RecyclerView.Adapter<KamusIEAdapter.KamusHolder> {

    List<KamusModelIE> mDataIE = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;

    public KamusIEAdapter(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public KamusHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_kamus_row, viewGroup, false);
        return new KamusHolder(view);
    }

    @Override
    public void onBindViewHolder(KamusHolder kamusHolder, int i) {
        kamusHolder.tvKata.setText(mDataIE.get(i).getKata());
//        kamusHolder.tvMakna.setText(mDataIE.get(i).getMakna());
    }

    public void addItem(List<KamusModelIE> mData) {
        this.mDataIE = mData;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDataIE.size();
    }

    public static class KamusHolder extends RecyclerView.ViewHolder {

        private TextView tvKata;
        private TextView tvMakna;

        public KamusHolder(View itemView) {
            super(itemView);
            tvKata = (TextView) itemView.findViewById(R.id.tv_kata);
//            tvMakna = (TextView) itemView.findViewById(R.id.tv_makna);
        }
    }

}
