package com.example.am.mykamus;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import com.example.am.mykamus.Database.KamusHelper;
import com.example.am.mykamus.Model.KamusModelEI;
import com.example.am.mykamus.Model.KamusModelIE;
import com.example.am.mykamus.Prefs.AppPreference;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String TagLog = "preload";
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        new LoadData().execute();
    }

    private class LoadData extends AsyncTask<Void, Integer, Void> {
        final String TAG = LoadData.class.getSimpleName();
        KamusHelper kamusHelper;
        AppPreference appPreference;
        double progressTotal;
        double progressIE;
        double progressEI;
        double maxprogress = 100;

        @Override
        protected void onPreExecute() {
            Log.d(TagLog, "onnPreExecute Main Aactivity");
            kamusHelper = new KamusHelper(MainActivity.this);
            appPreference = new AppPreference(MainActivity.this);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Log.d(TagLog, "doInBackground Main Aactivity");

            Boolean firsRun = appPreference.getFirstRun();
            if (firsRun) {
                ArrayList<KamusModelIE> kamusModels = preloadRawIE();
                ArrayList<KamusModelEI> kamusModels2 = preloadRawEI();

                kamusHelper.open();

                progressTotal = progressIE+progressEI;
                progressIE = 0;
                progressEI = 0;
                publishProgress((int) progressIE);
                Double progressMaxInsert = 80.0;
                Double progressDiff = (progressMaxInsert - progressTotal) / (kamusModels.size()+kamusModels2.size());

                kamusHelper.beginTransaction();

                try {

                    for (KamusModelIE model : kamusModels) {
                        kamusHelper.insertTransactionIE(model);
                        progressIE += progressDiff;
                        publishProgress((int) progressIE);
                    }

                    for (KamusModelEI model : kamusModels2) {
                        kamusHelper.insertTransactionEI(model);
                        progressEI += progressDiff;
                        publishProgress((int) (progressIE+progressEI));
                    }

                    kamusHelper.setTransactionSuccess();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                kamusHelper.endTransaction();
                kamusHelper.close();
                appPreference.setFirstRun(false);
                publishProgress((int) maxprogress);

            } else {
                try {
                    synchronized (this) {
                        this.wait(1000);
                        publishProgress(50);
                        this.wait(1000);
                        publishProgress((int) maxprogress);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Log.d(TagLog, "onProgressUpdate Main Aactivity");
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(TagLog, "onPostExecute Main Aactivity");

            Intent i = new Intent(MainActivity.this, KamusActivity.class);
            startActivity(i);
            finish();
        }

    }

    public ArrayList<KamusModelIE> preloadRawIE() {
        Log.d(TagLog, "preloadRawIE Main Aactivity");

        ArrayList<KamusModelIE> kamusModels = new ArrayList<>();
        String line = null;
        BufferedReader reader;
        try {
            Resources res = getResources();
            InputStream raw_dict_ie = res.openRawResource(R.raw.indonesia_english);

            reader = new BufferedReader(new InputStreamReader(raw_dict_ie));
            int count = 0;
            do {
                line = reader.readLine();
                String[] splitstr = line.split("\t");

                KamusModelIE kamusModel;

                kamusModel = new KamusModelIE(splitstr[0], splitstr[1]);
                kamusModels.add(kamusModel);
                count++;
            } while (line != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kamusModels;
    }

    public ArrayList<KamusModelEI> preloadRawEI() {
        Log.d(TagLog, "preloadRawIE Main Aactivity");

        ArrayList<KamusModelEI> kamusModels = new ArrayList<>();
        String line = null;
        BufferedReader reader, reader2;
        try {
            Resources res = getResources();
//            InputStream raw_dict_ie = res.openRawResource(R.raw.indonesia_english);
            InputStream raw_dict_ei = res.openRawResource(R.raw.english_indonesia);

//            reader = new BufferedReader(new InputStreamReader(raw_dict_ie));
            reader2 = new BufferedReader(new InputStreamReader(raw_dict_ei));
            int count = 0;
            do {
                line = reader2.readLine();
                String[] splitstr = line.split("\t");

                KamusModelEI kamusModel;

                kamusModel = new KamusModelEI(splitstr[0], splitstr[1]);
                kamusModels.add(kamusModel);
                count++;
            } while (line != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kamusModels;
    }
}
